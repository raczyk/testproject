import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Animation from "lottie-react-native";
import anim from "./soda_loader";

class Splash extends Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		this.animation.play();
	}
	render() {
		return (
			<View style={styles.container}>
				<View>
					<Animation
						ref={animation => {
							this.animation = animation;
						}}
						style={styles.animation}
						resizeMode="cover"
						source={anim}
					/>
				</View>
			</View>
		);
	}
}

export { Splash };

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: "#4286f4"
	},
	animation: {
		width: 100,
		height: 100
	}
});
